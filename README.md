### streamrecorder
Streamrcorder ist ein Skript zur Steuerung des Aufnahme-Plugins von TV-Browser.  
Mit streamrecorder kann man Sendungen die in der Programmzeitschrift TV-Browser angekündigt werden zeitgesteuert aufnehmen.
#### 1. Installation
Benötigt wird die Installation der Pakete `at` und `ffmpeg`.  
Zur Aktivierung von at wird auf der Konsole `sudo systemctl enable --now atd.service` ausgeführt.  
Das Skript wird als Datei `/usr/local/bin/streamrecorder` gespeichert und anschließend mit `chmod +x /usr/local/bin/streamrecorder` ausführbar gemacht.
#### 2. Konfiguration
Zuerst muss das Plugin Aufnahmesteuerung im TV-Browser geladen werden.  
Danach wird im Menu Extras -> Aufnahmesteuerung -> Geräte -> Gerät-hinzufügen gewählt.
##### 2.1  Reiter - Applikation
Im Feld Name wird `streamrecorder` eingetragen.  
Als Applikation wird das gespeicherte Skript streamrecoder mit Pfad angegeben.
##### 2.2  Reiter - Parameter
Damit TV-Browser Informationen zur jeweiligen Sendung an das Skript übergeben kann müssen Parameter angegeben werden. 
##### Aufnahme:
record {start_hour} {start_minute} {start_year} {start_month} {start_day} {end_hour} {end_minute} {end_year} {end_month} {end_day} "{channel_name}" "{title}" {production_year} {episode_number} "{episode}"
##### Löschen:
delete {start_hour} {start_minute} {start_year} {start_month} {start_day} {end_hour} {end_minute} {end_year} {end_month} {end_day} "{channel_name}" "{title}" {production_year} {episode_number} "{episode}"
##### 2.3  Reiter - Einstellungen
"Mehere Aufnahmen gleichzeitig" wird von skript unterstützt und kann ausgewählt werden.  
Auch können Aufnahmenen von schon "laufenden Sendungen" gemacht werden. Angefangene Sendungen werden vom Skript mit dem Namenszusatz "-part" gespeichert.
#### 3. Benutzung
Die Benutzung ist recht einfach. Mit der rechten Maustaste wählt man eine Sendung aus. Mit der linken Maustaste wird auf Aufnahme gedrückt. Das Löschen eines Aufnahmejobs funktioniert entsprechend.
#### 4. Weiters
Einige Sender URLs der öffentlich rechtlichen Sender in der BRD sind im Skript gelistet (Stand Juni 2023). Man kann beliebig viele andere Sender hinzufügen. Die Namen der Sender sollten dann im TV-Browser und im Skript jeweils angepasst werden.

Werden aufgenommene Titel vom user in einen Ordner `~/Videos/Filme` verlegt, um z.B. eine Filme-Sammlung anzulegen, verhindert das Skript weitere
Aufnehme-Jobs für diesen Titel. Der Pfad für Aufnahmen und Fimn-Sammlung kann im Skript angepasst werden.
